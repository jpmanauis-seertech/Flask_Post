from flask import Flask, g
from flask import request, render_template
from pymongo import MongoClient
import config
from config import MyDatabase
from flask import redirect


app = Flask(__name__)

DbClass = MyDatabase()
myDb = DbClass.createConn()

@app.route("/")
def index():
	post_list = DbClass.getData(myDb)
	return render_template('index.html',posts=post_list)
	#return render_template('index.html')

@app.route("/", methods=['POST'])
def create_and_post():

	if "new_post" in request.form:
		newEntry = request.form["new_post"]
		##INSERT NEW POST
		DbClass.insertData(newEntry,myDb)

		##GET LIST OF POSTS
		post_list = DbClass.getData(myDb)
		return render_template('index.html',posts=post_list)
	elif "update_post" in request.form:
		hash_data = {}
		if "delete" in request.form:
			delEntry = request.form["update_post"]
			DbClass.deleteData(myDb,delEntry)
		elif "like" in request.form:
			hash_data = {'like':1}
			row_id = request.form["update_post"]
			updateEntry = request.form["like"]
			DbClass.updateData(myDb,row_id,hash_data)
		elif "angry" in request.form:
			hash_data = {'angry':1}
			row_id = request.form["update_post"]
			updateEntry = request.form["angry"]
			DbClass.updateData(myDb,row_id,hash_data)
		elif "haha" in request.form:
			hash_data = {'haha':1}
			row_id = request.form["update_post"]
			updateEntry = request.form["haha"]
			DbClass.updateData(myDb,row_id,hash_data)
		elif "cry" in request.form:
			hash_data = {'cry':1}
			row_id = request.form["update_post"]
			updateEntry = request.form["cry"]
			DbClass.updateData(myDb,row_id,hash_data)

		##GET LIST OF POSTS
		post_list = DbClass.getData(myDb)
		return render_template('index.html',posts=post_list)


if __name__ == "__main__":

	app.run(debug=True)
	
