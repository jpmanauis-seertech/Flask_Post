from flask import Flask,g
from pymongo import MongoClient

class MyDatabase():
	def createConn(self):
		client = MongoClient()
		myDb = client.my_database

		return myDb

	def insertData(self,data,dbCon):
		row = dbCon.my_post.find_one(sort=[("rowid",-1)])
		row_count = 1
		if not row:
			row_count = 1
		else:
			row_count = int(row["rowid"]) + 1

		dbCon.my_post.insert(
			{
			"post" : data,
			"rowid" : row_count,
			"Like" : 0,
			"Angry" : 0,
			"Haha" : 0,
			"Cry" : 0
			})

	def getData(self,dbCon):
		row_list = dbCon.my_post.find()
		for row in row_list:
			print row
		return dbCon.my_post.find()
		#postlist = dbCon.my_post.find()
		#postDict = {}
		#for post in postlist:
		#	postDict['post']
		
	def deleteData(self,dbCon,row_id):

		print "ROW_ID : ",row_id
		try:
			status = dbCon.my_post.remove({"rowid" : int(row_id)})
		except Exception,err:
			print err

	def updateData(self,dbCon,rowid,hash_data):

		if hash_data.has_key('like'):
			count = dbCon.my_post.find({"rowid":int(rowid)})
			for info in count:
				if info["rowid"] != int(rowid): continue

				like = int(info["Like"])
				like += 1
				print "LIKES : ",like
				dbCon.my_post.update_one({"rowid":int(rowid)},{"$set":{"Like":int(like)}})
		elif hash_data.has_key('angry'):
			count = dbCon.my_post.find({"rowid":int(rowid)})
			for info in count:
				if info["rowid"] != int(rowid): continue

				angry = info["Angry"]
				angry += 1
				dbCon.my_post.update_one({"rowid":int(rowid)},{"$set":{"Angry":int(angry)}})
		elif hash_data.has_key('haha'):
			count = dbCon.my_post.find({"rowid":int(rowid)})
			for info in count:
				if info["rowid"] != int(rowid): continue

				haha = info["Haha"]
				haha += 1
				dbCon.my_post.update_one({"rowid":int(rowid)},{"$set":{"Haha":int(haha)}})
		elif hash_data.has_key('cry'):
			count = dbCon.my_post.find({"rowid":int(rowid)})
			for info in count:
				if info["rowid"] != int(rowid): continue

				cry = info["Cry"]
				cry += 1
				dbCon.my_post.update_one({"rowid":int(rowid)},{"$set":{"Cry":int(cry)}})

